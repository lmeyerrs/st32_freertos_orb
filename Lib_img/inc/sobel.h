/*
 * sobel.h
 *
 *  Created on: 7 de jun de 2020
 *      Author: lipem
 */

#ifndef INC_SOBEL_H_
#define INC_SOBEL_H_

#include "stm32f4xx_hal.h"
#include "img.h"

uint8_t calc_sobel_y_kernel_3(img_t *img, int16_t i, int16_t j);
void process_sobel_x(img_t *img_src, img_t *img_dst);
void process_sobel_y(img_t *img_src, img_t *img_dst);
void process_sobel_xy(img_t *img_x, img_t *img_y, img_t *img_out);

#endif /* INC_SOBEL_H_ */
