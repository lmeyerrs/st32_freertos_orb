/*
 * gaussean.h
 *
 *  Created on: Jun 5, 2020
 *      Author: lipem
 */

#ifndef LIB_IMG_INC_GAUSSEAN_H_
#define LIB_IMG_INC_GAUSSEAN_H_

#include "img.h"

#define GAUSSEAN_KERNEL_3	3
#define GAUSSEAN_KERNEL_5	5

#define GAUSSEAN_GRADIENT_COEF_3 16
#define GAUSSEAN_GRADIENT_COEF_5 159


void calc_gaussean_3(img_t *img_src, img_t *img_dst);
int8_t calc_gaussean(img_t *img_src, img_t *img_dst, uint8_t kernel);
#endif /* LIB_IMG_INC_GAUSSEAN_H_ */
