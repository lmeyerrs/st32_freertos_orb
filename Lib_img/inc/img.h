/*
 * img.h
 *
 *  Created on: Jun 5, 2020
 *      Author: lipem
 */

#ifndef LIB_IMG_INC_IMG_H_
#define LIB_IMG_INC_IMG_H_

#include "stm32f4xx_hal.h"

#define IMG_WIDTH 	64
#define IMG_HEIGTH	64

#define MAX_IMAGES_TO_PROCESS	1
#define BUFFER_SIZE	IMG_WIDTH*IMG_HEIGTH

typedef uint8_t Arr2DImg[IMG_WIDTH][IMG_HEIGTH];

typedef enum
{
	NO_STATE,
	PROCESS_GAUSSEAN,
	PROCESS_SOBEL_X,
	PROCESS_SOBEL_Y,
	PROCESS_SOBEL,
	PROCESS_CANNY
}img_state_t;

typedef struct
{
	uint16_t id;
	uint16_t width;
	uint16_t height;
	Arr2DImg *data;
}img_t;

typedef struct
{
	img_t image;
	img_state_t state;
}img_inf_t;

typedef struct
{
	int16_t num_imgs;
	img_inf_t images_in[MAX_IMAGES_TO_PROCESS];
	img_inf_t images_out[MAX_IMAGES_TO_PROCESS];
}img_ctrl_t;

void img_config(img_t *img_src, img_t *img_dst);
void img_init_ctrl(img_ctrl_t *img_ctrl);

#endif /* LIB_IMG_INC_IMG_H_ */
