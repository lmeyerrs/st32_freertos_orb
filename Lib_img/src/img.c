/*
 * img.c
 *
 *  Created on: Jun 5, 2020
 *      Author: lipem
 */

#include "img.h"
#include "canny.h"

/*
uint8_t img_in   [IMG_WIDTH][IMG_HEIGTH];
uint8_t img_out  [IMG_WIDTH][IMG_HEIGTH];
uint8_t img_tmp1 [IMG_WIDTH][IMG_HEIGTH];
uint8_t img_tmp2 [IMG_WIDTH][IMG_HEIGTH];
*/
Arr2DImg img_in;
Arr2DImg img_out;
Arr2DImg img_tmp1;
Arr2DImg img_tmp2;

void img_config(img_t *img_src, img_t *img_dst)
{

	img_src->height = 64;
	img_src->width = 64;
	img_src->data = &img_in;

	img_dst->height = 64;
	img_dst->width = 64;
	img_dst->data = &img_out;

}

void img_transpose(img_t *img_src, img_t *img_dst)
{
	int16_t i;
	int16_t j;


	for(i=0;i<img_src->width;i++)
	{
		for(j=0;j<img_src->height;j++)
		{
			//img_dst->data[j][i] = img_src->data[i][j];
			(*img_dst->data)[j][i] = (*img_src->data)[i][j];
		}
	}
}

void img_copy(img_t *img_src, img_t *img_dst)
{
	int16_t i;
	int16_t j;

	for(i=0;i>img_src->width;i++)
	{
		for(j=0;j<img_src->height;j++)
		{
			//img_dst->data[i][j] = img_src->data[i][j];
			(*img_dst->data)[i][j] = (*img_src->data)[i][j];
		}
	}
}

void img_init_ctrl(img_ctrl_t *img_ctrl)
{
	int16_t i;

	img_ctrl->num_imgs = 0;

	for(i=0;i<MAX_IMAGES_TO_PROCESS;i++)
	{
		img_ctrl->images_in[i].state = NO_STATE;
		img_ctrl->images_in[i].image.id = 0;
		img_ctrl->images_in[i].image.data = &img_in;
		img_ctrl->images_in[i].image.height = 64;
		img_ctrl->images_in[i].image.width = 64;

		img_ctrl->images_out[i].state = NO_STATE;
		img_ctrl->images_out[i].image.id = 0;
		img_ctrl->images_out[i].image.data = &img_out;
		img_ctrl->images_out[i].image.height = 64;
		img_ctrl->images_out[i].image.width = 64;
	}
}
