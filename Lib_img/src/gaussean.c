/*
 * gaussean.c
 *
 *  Created on: Jun 5, 2020
 *      Author: lipem
 */

#include "img.h"
#include "gaussean.h"


uint8_t gaussean_kernel_3 [3][3] = {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}};
uint8_t gaussean_kernel_5 [5][5] = {{2, 4, 5, 4, 2}, {4, 9, 12, 9, 4}, {5, 12, 15, 12, 5}, {4, 9, 12, 9, 4}, {2, 4, 5, 4, 2}};


static inline uint8_t calc_gaussean_kernel_3(img_t *img, int16_t i, int16_t j)
{
	uint8_t result;
	float temp;

	temp = ((*img->data)[i][j]     *  gaussean_kernel_3[0][0] +
			(*img->data)[i][j+1]   *  gaussean_kernel_3[0][1] +
			(*img->data)[i][j+2]   *  gaussean_kernel_3[0][2] +
			(*img->data)[i+1][j]   *  gaussean_kernel_3[1][0] +
			(*img->data)[i+1][j+1] *  gaussean_kernel_3[1][1] +
			(*img->data)[i+1][j+2] *  gaussean_kernel_3[1][2] +
			(*img->data)[i+2][j]   *  gaussean_kernel_3[2][0] +
			(*img->data)[i+2][j+1] *  gaussean_kernel_3[2][1] +
			(*img->data)[i+2][j+2] *  gaussean_kernel_3[2][2])/GAUSSEAN_GRADIENT_COEF_3;

	result = (uint8_t)temp;
	return result;
}

void calc_gaussean_3(img_t *img_src, img_t *img_dst)
{
	int16_t i;
	int16_t j;


	for(i=0;i<(img_src->width - GAUSSEAN_KERNEL_3);i++)
	{
		for(j=0;j<(img_src->height - GAUSSEAN_KERNEL_3);j++)
		{
			(*img_dst->data)[i][j] = calc_gaussean_kernel_3(img_src, i, j);
		}
	}

}

void calc_gaussean_5(img_t *img, img_t *img_dst)
{

}

int8_t calc_gaussean(img_t *img_src, img_t *img_dst, uint8_t kernel)
{
	int8_t ret = 0;

	if (kernel == 3)
	{
		calc_gaussean_3(img_src, img_dst);
		ret = 3;
	}
	else if(kernel == 5)
	{
		calc_gaussean_5(img_src, img_dst);
		ret = 3;
	}
	else
	{
		ret = -1;
	}
	return ret;
}
