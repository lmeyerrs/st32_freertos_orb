/*
 * sobel.c
 *
 *  Created on: 7 de jun de 2020
 *      Author: lipem
 */

#include "sobel.h"
#include "stm32f4xx_hal.h"
#include "img.h"

extern Arr2DImg img_in;
extern Arr2DImg img_out;
extern Arr2DImg img_tmp1;
extern Arr2DImg img_tmp2;


uint8_t sobel_x [3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
uint8_t sobel_y [3][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};


static inline uint8_t calc_sobel_x_kernel_3(img_t *img, int16_t i, int16_t j)
{
	uint8_t result;
	float temp;

	temp =  (*img->data)[i-1][j-1]*sobel_x[i-1][j-1] +
			(*img->data)[i-1][j]*sobel_x[i-1][j-1] +
			(*img->data)[i-1][j+1]*sobel_x[i-1][j-1] +
			(*img->data)[i][j-1]*sobel_x[i-1][j-1] +
			(*img->data)[i][j]*sobel_x[i-1][j-1] +
			(*img->data)[i][j+1]*sobel_x[i-1][j-1] +
			(*img->data)[i+1][j-1]*sobel_x[i-1][j-1] +
			(*img->data)[i+1][j]*sobel_x[i-1][j-1] +
			(*img->data)[i+1][j+1]*sobel_x[i-1][j-1];

	if(temp<0)
		result = (uint8_t)-1*temp;
	else if (temp>255)
		result = 255;
	else
		result = (uint8_t)temp;

	return result;
}


uint8_t calc_sobel_y_kernel_3(img_t *img, int16_t i, int16_t j)
{
	uint8_t result;
	float temp;

	temp =  (*img->data)[i-1][j-1]*sobel_y[i-1][j-1] +
			(*img->data)[i-1][j]*sobel_y[i-1][j-1] +
			(*img->data)[i-1][j+1]*sobel_y[i-1][j-1] +
			(*img->data)[i][j-1]*sobel_y[i-1][j-1] +
			(*img->data)[i][j]*sobel_y[i-1][j-1] +
			(*img->data)[i][j+1]*sobel_y[i-1][j-1] +
			(*img->data)[i+1][j-1]*sobel_y[i-1][j-1] +
			(*img->data)[i+1][j]*sobel_y[i-1][j-1] +
			(*img->data)[i+1][j+1]*sobel_y[i-1][j-1];

	if(temp<0)
		result = (uint8_t)-1*temp;
	else if (temp>255)
		result = 255;
	else
		result = (uint8_t)temp;

	return result;
}



void process_sobel_x(img_t *img_src, img_t *img_dst)
{
	int16_t i;
	int16_t j;


	for(i=0;i<img_src->width;i++)
	{
		for(j=0;j<img_src->height;j++)
		{
			(*img_dst->data)[i][j] = calc_sobel_x_kernel_3(img_src, i, j);
		}
	}
}

void process_sobel_y(img_t *img_src, img_t *img_dst)
{
	int16_t i;
	int16_t j;

	for(j=0;j<img_src->height;j++)
	{
		for(i=0;i<img_src->width;i++)
		{
			(*img_dst->data)[i][j] = calc_sobel_y_kernel_3(img_src, i, j);
		}
	}
}


void process_sobel_xy(img_t *img_x, img_t *img_y, img_t *img_out)
{
	int16_t i = 0;
	int16_t j = 0;

	for(i=0;i<img_out->width;i++)
	{
		for(j=0;j<img_out->height;j++)
		{
			(*img_out->data)[i][j] = (*img_x->data)[i][j] + (*img_y->data)[i][j];
			if((*img_out->data)[i][j] > 255)
				(*img_out->data)[i][j] = 255;
			else if((*img_out->data)[i][j] < 0)
				(*img_out->data)[i][j] = 0;
		}
	}
}


void process_sobel(img_t *img_in, img_t *img_out)
{
	img_t *img_x = NULL;
	img_t *img_y = NULL;

	img_x->width = img_in->width;
	img_x->height = img_in->height;
	img_x->data = (Arr2DImg *)img_tmp1;

	img_y->width = img_in->width;
	img_y->height = img_in->height;
	img_y->data = (Arr2DImg *)img_tmp2;

	process_sobel_x(img_in, img_x);
	process_sobel_y(img_in, img_y);
	process_sobel_xy(img_x, img_y, img_out);
}
